## **Processamento de Logs da Nasa** 

Este desafio foi resolvido usando sparkSQL para limpeza e adequação dos dados e Dataframes para responder as perguntas propostas.

O desafio consiste em responder as seguintes questões:

- Número de hosts únicos.    
- O total de erros 404.  
- Os 5 URLs que mais causaram erro 404.  
- Quantidade de erros 404 por dia.  
- O total de bytes retornados.


Fonte oficial do dataset https://ita.ee.lbl.gov/html/contrib/NASA-HTTP.html  

Dados:  
ftp://ita.ee.lbl.gov/traces/NASA_access_log_Jul95.gz  
Sobre o dataset:  
O conjuto de dados possui todas as requisições HTTP para o servidor da NASA Kennedy Space Center WWW na Flórida para um período específico.